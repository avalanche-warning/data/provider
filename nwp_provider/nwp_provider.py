"""
This module provides numerical weather prediction (NWP) data for specific domains within AWSOME.
It supports both local datasets (netcdf) and databases, and knows where to search for the data based on the 
provided meteo source in the domain.xml file. To extract the data, it relies on custom NWP processor plugins
that are tailored to the specific NWP sources (only for local datasets).

The module is designed to be flexible, allowing for different toolchains and NWP-specific settings. It can 
be run as a script or imported into Python.

Functionality:
--------------
- Extract and write NWP data from a database or through a specified NWP processor plugin into a standardized netcdf file readable by AWSOME's toolchains.
- Knows where to find the NWP datasets and can open them using xarray.
- Handle specific requirements of different meteo sources.

Usage:
------
1. Interactive Python usage:
        >>> import nwp_provider as awnwp
        >>> nwp = awnwp.open_dataset('domain', 'toolchain', {'nowcast'|'forecast'}, update_dataset=True, aux_config=config_runtime_domain)

        If `update_dataset` is set to `True`, the function `extract_write_data()` will be invoked, which calls the nwp processor plugins (for local datasets only).

2. Script:
    Script usage will invoke the function `extract_write_data()` by

    $ python3 nwp_provider.py domain toolchain [--domain-path] [--aux-config]


Dependencies:
-------------
The module relies on access to NWP data through custom NWP processor plugins or a custom database.
So for adding new NWP sources to AWSOME, please
1. provide a processor plugin that queries and preprocesses your custom NWP data formats (for local datasets only), for examples of existing nwp_processor plugins see https://gitlab.com/avalanche-warning/snow-cover/preprocessing/nwp-models.
2. add a database query function analogous to `_open_dataset_mssql()` to this module (for database use only)
3. add the specific settings for your meteo source to `_prepare_config()` in this module.

"""

import os
import sys
import argparse
import configparser
import subprocess
import sqlalchemy
import pyproj
import datetime as dt
import numpy as np
import pandas as pd
import xarray as xr
from typing import Literal, Optional

import awio
from awset import get, getattribute, get_crs, get_elev_bands, DEFAULT_DOMAIN_PATH, awsome_env
from nwp_provider.helpers import _NWP_DIR_NC, _NWP_DIR_FC, _NWP_FNAME_NC, _NWP_FNAME_FC, \
    interpolate_temporally, add_crs_attribute, transform_wkt_crs

# TODO: 
# * allow for cropping of lat/lon or time?


def extract_write_data(domain:str, toolchain:str, domain_path:str=DEFAULT_DOMAIN_PATH, aux_config:configparser.ConfigParser|None=None, **kwargs):
    """
    Extract and process NWP data for a given domain and save the resulting netcdf file at 
        `~snowpack/{toolchain}/data/{meteo_source}/nwp_{nowcast|forecast}-{domain}.nc`. 
    For the local datasets approach, this involves calling the NWP processor plugin. For the database approach, this queries the db.
    Nowcasts will always be updated, forecasts only if enabled in domain.xml file.
    
    Parameters:
    -----------
        domain (str): The domain identifier.
        toolchain (str): Specifies the toolchain, currently {'gridded-chain', 'snowobs-chains'}.
        domain_path (str): Path to the domain configuration folder.
        aux_config (configparser.ConfigParser, optional): Additional configuration details required by the specific NWP processor.

    Returns:
    --------
        ds (xr.DataSet): Only returns a dataset for database approaches, does not return anything for plugins!
    """
    config, kwargs = _prepare_config(domain, toolchain, domain_path, aux_config, **kwargs)

    """Extract NWP data from database and write to netcdf file"""
    if config.getboolean('General', 'database'):
        function_map = _create_function_map()
        ds = function_map[config.get('Custom_args_nwp_processor', '_open_dataset_function')](config, 'nowcast', **kwargs)
        write_netcdf_safely(ds, config.get('Paths', 'nwp_nowcast_fpath'))
        if config.get('General', 'meteo_source_fc') != 'None':
            ds = function_map[config.get('Custom_args_nwp_processor', '_open_dataset_function')](config, 'forecast', **kwargs)
            write_netcdf_safely(ds, config.get('Paths', 'nwp_forecast_fpath'))
        return ds
    
    else:
        """Call NWP processor plugin, which queries/preprocesses/writes NWP data to netcdf file"""
        command = [sys.executable, config.get('Paths','nwp_processor')]
        for arg, value in config.items('Global_args_nwp_processor'):
            command.append(value)
        if config.get('Global_args_nwp_processor', 'meteo_source_fc') != 'None':
            command.append('--update-forecast')
        if config.has_section('Custom_args_nwp_processor'):
            for arg, value in config.items('Custom_args_nwp_processor'):
                command.append(arg)
                command.append(value)
        
        returnCode = subprocess.call(command)
        if returnCode == 0:
            print("[i]  NWP data updated successfully.")
        else:
            print("[E]  Updating of NWP data not successful!")
            print(f"Return code: {returnCode}")
            sys.exit(1)


def open_dataset(
   domain:str,
   toolchain:str,
   cast_type:str,
   band:Optional[str]=None,
   update_dataset:bool=True,
   aux_config:Optional[configparser.ConfigParser]=None,
   domain_path:str=DEFAULT_DOMAIN_PATH,
   prefer_nc_over_db=False,
   **kwargs
):
    """
    Opens an xarray dataset either from file or database based on meteo source in domain.xml. 
    It can also update the local dataset on file by re-running the NWP processor.
    
    Parameters:
    -----------
        domain (str): 
            The domain identifier.
        toolchain (str): 
            Specifies the toolchain, currently {'gridded-chain', 'snowobs-chains'}.
        cast_type (str): 
            Type of NWP data, e.g., {'nowcast', 'forecast'}.
        band: (str, optional):
            Filter for a specific elevation band name (currently only implemented for DB approach).
        update_dataset (bool): 
            Whether to update the local netcdf dataset by re-running the NWP processor. 
            For database approaches: if the domain.xml <meteo><delivery> section is set to SMET, 
            this will dump the dataset to a local netcdf file.
        aux_config (configparser.ConfigParser, optional): 
            Auxiliary configuration.
        domain_path (str): 
            Path to the domain configuration folder.
        prefer_nc_over_db (bool):
            for DB scenarios, you can open the dataset directly from the DB (False) or from a netcdf file if it was generated at a prior step (True)
        **kwargs:
            Key-word-arguments passed to the underlying function.
            Implementation-specific, e.g. see elevation and
            spatial_filter in _open_dataset_mssql()
    Returns:
        xarray.Dataset: The resulting dataset, currently all grid points and time steps.
    """
    config, kwargs = _prepare_config(domain, toolchain, domain_path, aux_config, band, **kwargs)
    function_map = _create_function_map()
    if not prefer_nc_over_db and config.getboolean('General', 'database'):
        if update_dataset and (get(['gridded-chain', 'meteo', 'delivery'], domain, domain_path) == 'SMET'):
            ds = extract_write_data(domain, toolchain, domain_path, aux_config, **kwargs)
        else:
            ds = function_map[config.get('Custom_args_nwp_processor', '_open_dataset_function')](config, cast_type, **kwargs)
    else:
        if update_dataset:
            extract_write_data(domain, toolchain, domain_path, aux_config, **kwargs)
        ds = xr.open_dataset(
            config.get('Paths', f'nwp_{cast_type}_fpath'),
            # **kwargs  # Not necessarily needed at this stage, but causes 
            #             TypeError: NetCDF4BackendEntrypoint.open_dataset() got an unexpected keyword argument 'spatial_filter'
        )

    if 'execute_subfunctions' in config['General']:
        execute_subfunctions = config['General']['execute_subfunctions']
        execute_subfunctions = [func.strip() for func in execute_subfunctions.split(',')]
        for func in execute_subfunctions:
            ds = function_map[func](ds, **kwargs)
    
    # Sanity checks
    if ds['time'].shape[0] == 0 or ds['x'].shape[0] == 0 or ds['y'].shape[0] == 0:
        print("[W]  You are getting an empty NWP dataset!")
    if 'crs' in ds.attrs and config.has_option('General', 'meteo_source_crs'):
        if pyproj.CRS(ds.attrs['crs']) != pyproj.CRS(config.get('General', 'meteo_source_crs')):
            raise ValueError("CRS in NetCDF does not match awset.get_crs()!")

    return ds


def _prepare_config(domain:str, toolchain:str, domain_path:str|None, aux_config:configparser.ConfigParser|None, band:str|None=None, **kwargs):
    """
    Prepares and returns the configuration for NWP data processing based on domain and toolchain.
    """
    meteo_source = get([toolchain, "meteo", "source"], domain, domain_path)
    if meteo_source:
        meteo_source_nc = meteo_source
        meteo_source_fc = meteo_source
        meteo_sources = [meteo_source]
    else:
        meteo_source_nc = get([toolchain, "meteo", "nowcast", "source"], domain, domain_path, throw=True)
        meteo_source_fc = get([toolchain, "meteo", "forecast", "source"], domain, domain_path) \
            if getattribute([toolchain, "meteo", "forecast"], 'enabled', domain, domain_path) == "true" else None
        meteo_sources = [meteo_source_nc, meteo_source_fc]
        meteo_sources = [src for src in meteo_sources if src is not None]
    
    """Global settings identical for all nwp sources"""
    config             = configparser.ConfigParser()
    config.optionxform = str
    config['General'] = {
        'database'                      : 'FALSE',  # initialize
        'meteo_source_nc'               : str(meteo_source_nc),
        'meteo_source_fc'               : str(meteo_source_fc),
    }
    crs = get_crs(domain, 'wkt_string', toolchain=toolchain, throw=False, cfgpath=domain_path)
    if crs is not None:
        config['General']['meteo_source_crs'] = crs
    _nwp_dir_nc = eval(_NWP_DIR_NC)
    _nwp_dir_fc = eval(_NWP_DIR_FC)
    os.makedirs(_nwp_dir_nc, exist_ok=True)
    if meteo_source_fc:
        os.makedirs(_nwp_dir_fc, exist_ok=True)
    config['Paths'] = {
        'nwp_nowcast_fpath'             : os.path.join(_nwp_dir_nc, eval(_NWP_FNAME_NC)),
        'nwp_forecast_fpath'             : os.path.join(_nwp_dir_fc, eval(_NWP_FNAME_FC)) if meteo_source_fc else 'None'
    }
    config['Global_args_nwp_processor'] = {
        'meteo_source_nc'               : str(meteo_source_nc),
        'meteo_source_fc'               : str(meteo_source_fc),
        'date_opera'                    : get([toolchain, 'run', 'date_opera'], domain, domain_path),
        'season_start'                  : get([toolchain, 'run', 'season_start'], domain, domain_path),
        'season_end'                    : get([toolchain, 'run', 'season_end'], domain, domain_path),
        'configfile_private'            : awsome_env('AWSOME_SECRETS'),
        'nwp_nowcast_fpath'             : config.get('Paths', 'nwp_nowcast_fpath'),
        'nwp_forecast_fpath'            : config.get('Paths', 'nwp_forecast_fpath'),
        # '--update-forecast'               : boolean flag will be set during subprocess call based on meteo_source_fc
    }

    """Load kwargs"""
    # convert elevation band into elevation range
    if band:
        lower_limits, upper_limits, _generate_flags, band_names = get_elev_bands(domain, toolchain, cfgpath=domain_path)
        k = np.where(np.isin(band_names, band))[0]
        if k:
            kwargs['elevation_min'] = int(min(lower_limits[k]))
            kwargs['elevation_max'] = int(max(upper_limits[k]))

    """Individual settings of meteo sources: Here you can customize!"""
    if all(source in ["arome-tirol", "ecmwf-tirol"] for source in meteo_sources):
        config['Paths']['nwp_processor']        = os.path.join(awsome_env("AWSOME_BASE"), "snow-cover/preprocessing/nwp-models/geosphere_tirol_nwp_processor.py")
        config['Custom_args_nwp_processor'] = {}
    
    elif all(source in ["arome-norway"] for source in meteo_sources):
        config['Paths']['nwp_processor']        = os.path.join(awsome_env("AWSOME_BASE"), "snow-cover/preprocessing/nwp-models/nve_nwp_processor.py")
        if aux_config:
            config['Custom_args_nwp_processor'] = {
                '--vstations-csv-file': aux_config.get('Paths', '_vstations_csv_file')
            }
        config['General']['execute_subfunctions'] = 'add_crs_attribute'
        if config.has_option('General', 'meteo_source_crs') and 'crs' not in kwargs:
            kwargs['crs'] = config.get('General', 'meteo_source_crs')
    
    elif all(source in ["mssql-norway"] for source in meteo_sources):
        config['General']['database']           = 'TRUE'
        config['Custom_args_nwp_processor'] = {
            '_open_dataset_function'    : '_open_dataset_mssql'
        }
        kwargs['verbose'] = True
        kwargs['view'] = 'awsome.input_scalar_latest_view_100m_offset'
        if aux_config and 'spatial_filter' not in kwargs:
            kwargs['spatial_filter'] = aux_config.get('SpatialFilter', 'wkt')
            kwargs['spatial_filter_crs'] = aux_config.get('SpatialFilter', 'wkt_crs')
        config['General']['execute_subfunctions'] = 'interpolate_temporally, add_crs_attribute'  # comma-separated list of subfunctions to call
        if config.has_option('General', 'meteo_source_crs') and 'crs' not in kwargs:
            kwargs['crs'] = pyproj.CRS(config.get('General', 'meteo_source_crs')).to_epsg()
            if kwargs['crs'] != 25833:
                raise ValueError("NVE MSSQL DB used to be set up with EPSG 25833. Investigate!")
        if 'spatial_filter_crs' in kwargs and kwargs['spatial_filter_crs'] not in [None, 'None'] and kwargs['crs'] != kwargs['spatial_filter_crs']:
            raise ValueError("Need to convert wkt crs before querying DB!")

    else:
        raise ValueError("NWP model source not implemented or combination of nowcast--forecast sources not supported (all sources need to be processed by the same nwp processor plugin). ")

    config = awio.expanduser(config)
    return config, kwargs


def _create_function_map(include=None):
    """
    Automatically generates a function map of all callable objects
    that are directly defined in this module (not imported).
    
    Optionally includes functions from the module passed in the `include` argument.
    
    Parameters
    ----------
    include : module, optional
        A Python module (like `helpers`) to include functions from.
    
    Returns
    -------
    dict : A dictionary where the keys are function names and values are function objects.
    """
    current_module = sys.modules[__name__]
    
    # Get functions from the current module
    function_map = {name: obj for name, obj in globals().items()
                    if callable(obj) and getattr(sys.modules[obj.__module__], name) == obj}
    
    # If include is provided, add functions from the provided module
    if include is not None:
        external_functions = {name: obj for name, obj in vars(include).items() if callable(obj)}
        function_map.update(external_functions)
    
    return function_map

def write_netcdf_safely(ds, fpath):
    """Write netcdf file, while avoiding file lock issues that cause
    PermissionDeniedErrors with xarray, by renaming existing file before overwriting. 
    .backup file will be deleted upon successful saving.
    """
    backup_path = f"{fpath}.backup"
    if os.path.exists(fpath):
        os.rename(fpath, backup_path)
    ds.to_netcdf(fpath)
    if os.path.exists(backup_path):
        os.remove(backup_path)

def _open_dataset_mssql(
        config: configparser.ConfigParser,
        cast_type: Literal['nowcast', 'forecast', 'nowcast+forecast']='nowcast+forecast',
        #elevation: Optional[int]=None,  # can be deprecated once min and max are being used
        elevation_min: Optional[int]=None, 
        elevation_max: Optional[int]=None,
        spatial_filter: Optional[str]=None,
        view: Optional[str]='awsome.input_scalar_latest_view',
        verbose: Optional[bool]=False,
        **kwargs
        ) -> xr.Dataset:
    """
    Run dataset query and return an xarray.Dataset of NWP data. 
    SQL query of this function is compatible with MSSQL databases.
    For more DB requirements, read below.

    Parameters:
    -----------
        config (configparser.ConfigParser):
            Configuration for NWP data processing.
        cast_type (str):
            Extract subset of dataset: {nowcast, forecast, nowcast+forecast}
        elevation (int):
            The elevation layer to fetch: {150, 450, 750, 1050, ..., 1950}.
            If None, all layers are returned in extra 'z' dim.
        spatial_filter (str):
            A WKT (https://en.wikipedia.org/wiki/Well-known_text) geometry.
            All database geometries intersecting with the spatial_filter are
            included in the returned dataset, e.g.:
              * 'LINESTRING (594500 7589500, 654500 7589500)'
              * 'MULTIPOINT ((594500 7589500), (654500 7589500))'
              * POLYGON, ... 
            Note that the POINTS need to match the DB geometries exactly to
            satisfy the spatial_filter (unless the DB geometries are defined as areas).
        view (str): The DB view to retrieve the data from.
        verbose (bool): Print DB query
    Returns:
    --------
        xarray.Dataset:
            The resulting dataset

    Database requirements:
    ----------------------
        1. Database view
            Per default, the function queries from a view named awsome.input_scalar_latest_view. 
            This view must be structured to contain the following columns: 
            time, x, y, z (elevation), geom (see spatial queries below), model_id (1 for reanalysis/nowcast, 0 for forecast), 
            and all meteorological variables listed in the domain.xml file using SNOWPACK's naming convention. The view can be
            changed based on an input parameter (see below).
        2. Spatial queries
            The view should support spatial queries. It assumes that there is a geom column formatted 
            in a spatial data type that can interact with WKT (Well-known Text) geometries for filtering purposes. 
            This requires the database to have spatial capabilities. Please ensure that the EPSG code (or proj4_string)
            given in the domain.xml file corresponds to the coordinate reference system used by the database.
        3. Temporal alignment
            The view should only return one row of data per time step, even if multiple rows are available (from multiple NWP model producst).
    """
    global_conf = config['Global_args_nwp_processor']
    private_config = configparser.ConfigParser()
    private_config_file = global_conf['configfile_private']
    private_config.read(private_config_file)
    
    params = {
        "spatial": spatial_filter,
        "epsg": kwargs['crs'],
        "elev_min": elevation_min,
        "elev_max": elevation_max,
        "start": global_conf['season_start'],
        "end": global_conf['season_end'],
    }
    spatial_where = """geom.STIntersects(geometry::STGeomFromText(
        %(spatial)s,
        %(epsg)s)
    ) = 1 AND """
    z_min_where = "%(elev_min)s < elevation_max AND "
    z_max_where = "%(elev_max)s > elevation_min AND "
    if cast_type == "nowcast":
        temporal_where = "time <= last_nowcast.last_time AND "
    elif cast_type == "forecast":
        temporal_where = "time > last_nowcast.last_time AND "
    elif cast_type == "nowcast+forecast":
        temporal_where = ""
    else:
        raise ValueError(f"Unknown cast_type: {cast_type}")

    connstr = private_config.get('NWP','_DB_connstr')
    engine = sqlalchemy.create_engine(connstr)
    with engine.connect() as conn:
        query = f"""
            WITH last_nowcast(last_time) AS (
                SELECT TOP 1 time as last_time
                FROM {view}
                WHERE model_id = 1
                ORDER BY time DESC
            )
            SELECT input_scalar.*
            FROM {view} input_scalar
            JOIN last_nowcast ON 1=1
            WHERE
                {spatial_where if spatial_filter else ''}
                {z_min_where if elevation_min else ''}
                {z_max_where if elevation_max else ''}
                {temporal_where}
                %(start)s <= input_scalar.time AND input_scalar.time < %(end)s
        """
        if verbose:
            print(query)
            print(params)
        index = ["x", "y", "z", "time"]
        result = pd.read_sql(query, con=conn, index_col=index, params=params)
    result.drop(
        columns= ["geom", "model_id", "elevation_min", "elevation_max"],
        inplace=True
    )
    result.index = result.index.set_levels([
        l.astype(float) if l.dtype == np.dtype('int64') else l
        for l in result.index.levels
    ])
    if len(result.index.unique("z")) <= 1:
        result.index = result.index.droplevel("z")
    engine.dispose()
    
    ds = result.to_xarray()
    return ds


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('domain', help='The domain for which to get NWP data')
    parser.add_argument('toolchain', choices=['gridded-chain', 'snowobs-chain'])
    parser.add_argument('--domain-path', default=DEFAULT_DOMAIN_PATH, help='Where to find the domain.xml file')
    parser.add_argument('--aux-config', default=None, help='Path to config file that contains auxiliary information required by specific nwp processor plugin')
    args = vars(parser.parse_args())

    if args['aux_config'] is not None:
        aux_config = configparser.ConfigParser()
        aux_config.read(args['aux_config'])
        extract_write_data(args['domain'], args['toolchain'], args['domain_path'], aux_config)
    else:
        extract_write_data(args['domain'], args['toolchain'], args['domain_path'])
