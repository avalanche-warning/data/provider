"""
The NWP SMET generator module provides functionality for generating SMET files from Numerical Weather Prediction (NWP) data for
Points of Interest (POIs).

Key Functions:
--------------
1. `generate_smets_for_pois`: 
    This function takes NWP data (as an xarray dataset or NetCDF file) and a list of POIs (as a CSV file or pandas DataFrame),
    and generates SMET files for each POI. The function is parallelized and represents the recommended way to generating many 
    SMET files from NWP data.

2. `generate_smet_single`:
    This function generates a SMET file from an existing NetCDF NWP data for a specific set of coordinates 
    and a time period. It is useful for extracting data for a single point location if it is unknown to the user *which* 
    NetCDF dataset contains the desired coordinates and time. The function will search in all available nc files if neccessary.

3. `df2smet`:
    This function writes the timeseries data extracted from NWP data for a single POI into a SMET file. It 
    handles the SMET format requirements, including writing the header and data fields.

Usage Example:
--------------
The smet_generator can be used from Python or be executed as a command-line script:

# Python:
>>> from nwp_provider.smet_generator import generate_smets_for_pois, generate_smet_single

# Example usage for generating SMET files for POIs from NWP data
>>> generate_smets_for_pois(
        ds_nwp="path/to/nwp_data.nc", 
        df_vstations="path/to/poi_data.csv", 
        smet_dir="output/smet_files", 
        meteo_variables="TA RH VW PSUM", 
        crs=pyproj.CRS("EPSG:4326"),
        ncpus=4
)

# Example usage for generating a single SMET file for a specific location and time period
>>> generate_smet_single(
        meteo_source="ecmwf-tirol", 
        x=123456, 
        y=654321, 
        datetime_start="2024-01-01 00:00:00", 
        datetime_end="2024-01-10 00:00:00", 
        out_path="path/to/output.smet"
)

# Script usage for generate_smets_for_pois from the command line
$ python3 smet_generator.py generate_smets_for_pois \
    path/to/nwp_data.nc path/to/poi_data.csv \
    output/smet_files --meteo_variables "TA RH VW PSUM" \
    --crs "EPSG:4326" --ncpus 4

# Script usage for generate_smet_single from the command line
$ python3 smet_generator.py generate_smet_single \
    ecmwf-tirol 123456 654321 \
    "2024-01-01 00:00:00" "2024-01-10 00:00:00" \
    "path/to/output.smet" --variables "TA RH VW PSUM"

"""

import argparse
from enum import IntEnum
import glob
import os
import pyproj
import sys
import time
import warnings

import multiprocessing as mp
import numpy as np
import pandas as pd
import xarray as xr

import awio
from awsmet import SMETParser
from nwp_provider.helpers import _NWP_DIR_NC, _NWP_DIR_FC, _NWP_FNAME_NC, _NWP_FNAME_FC

warnings.simplefilter(action='ignore', category=FutureWarning)

class DataError(IntEnum): # error return codes for NWP extraction
    OK = 0
    LOC_NA = 1
    TIME_NA = 2
    TIMESPAN_NA = 3
    DATA_NA = 4

_smet_template_path = awio.filetools.get_scriptpath(__file__) + "templates/template.smet"

def generate_smets_for_pois(ds_nwp, df_vstations, smet_dir: str, meteo_variables: str="default", 
                            crs: pyproj.CRS|None=None, ncpus=1, semidistributed: bool=False,
                            ds_nwp_prior=None):
    """
    Generate SMET files for a list of POIs (CSV-file) for the provided NWP data

    Parameters
    ----------
    ds_nwp : str or xarray.Dataset
        Either the path to a NetCDF file containing NWP data, or an already opened xarray dataset.
        
    df_vstations : str or pandas.DataFrame
        Either the path to a CSV file with POI information, or a pandas DataFrame containing the POIs.
        The CSV file must include either:
        - 'easting' and 'northing': Coordinates of the POIs in the same CRS as the NWP dataset.
        - 'lat' and 'lon': Latitude and longitude coordinates (the NetCDF file must contain information about
           the CRS, or a CRS must be provided to this function).
        Additionally, the following columns are used:
        - 'vstation': Used for naming the SMET files and POIs.
        - 'elev': Elevation of the POI, included in the SMET header.

    smet_dir : str
        The directory where the generated SMET files will be saved (i.e., filenames are auto-generated)
    
    meteo_variables : str, optional
        A space-separated string of meteorological variables to include in the SMET file.
        Common variables might include 'TA', 'RH', 'VW', 'DW', etc.

    crs : pyproj.CRS, optional
        Coordinate Reference System (CRS) of the POIs. If the POIs are provided as lat/lon, this CRS 
        is used to project the coordinates. Can be retrieved from NetCDF file itself, if it contains the
        attribute crs.

    ncpus : int, optional
        The number of CPUs to use for parallel SMET file generation.

    semidistributed : bool, optional
        Are the smets being generated for a semi-distributed model configuration (True) or for a fully-distributed one (False)?
    
    ds_nwp_prior : str or xarray.Dataset
        Either the path to a NetCDF file containing NWP data, or an already opened xarray dataset.
        Since SNOWPACK needs the last nowcast timestep also as first forecast timestep (if two separate SMET files), providing 
        this dataset of NWP data prior to `ds_nwp` allows the function to make sure the forecast SMET file will satisfy SNOWPACK.
    """

    ## - Allow for files or paths - ##
    if isinstance(ds_nwp, str):
        ds_nwp = xr.open_dataset(ds_nwp, engine="netcdf4")
    if isinstance(ds_nwp_prior, str):
        ds_nwp_prior = xr.open_dataset(ds_nwp, engine="netcdf4")  
    if isinstance(df_vstations, str):
        df_vstations = pd.read_csv(df_vstations)

    os.makedirs(smet_dir, exist_ok=True)

    ## - Get CRS information (in generic pyproj.CRS manner) - ##
    if 'crs' in ds_nwp.attrs.keys():
        crs_nc = pyproj.CRS(ds_nwp.attrs['crs'])
        epsg = crs_nc.to_epsg() or -999
        if crs is not None and crs_nc != crs:
            raise ValueError("Provided crs does not match crs contained in netCDF file!")
        else:
            crs = crs_nc
    if ds_nwp_prior is not None and 'crs' in ds_nwp_prior.attrs.keys():
        crs_nc_prior = pyproj.CRS(ds_nwp.attrs['crs'])
        if crs_nc_prior != crs:
            raise ValueError("crs from ds_nwp_prior does not match crs from ds_nwp!")
        
    if ds_nwp_prior is not None and ds_nwp.time.min() > ds_nwp_prior.time.max():
        ds_nwp = prepend_last_timestep(ds_nwp, ds_nwp_prior)

    progress_counter = mp.Manager().Value('i', 0)
    lock = mp.Manager().Lock()
    stime = time.time()
    pbar = {"progress_counter": progress_counter, "lock": lock, "stime": stime, "ntasks": len(df_vstations)}
    sema = mp.Semaphore(ncpus)

    ## - Select locations in NETCDF and generate SMETS - ##
    # TODO: We can try to make the selection in parallel, too, but that might also blow up RAM for large NETCDFs
    running_procs = []
    for vstation in df_vstations.to_dict(orient="records"):
        if "northing" in vstation.keys():
            xx = vstation['easting']
            yy = vstation['northing']
            if semidistributed:
                zz = vstation['elev']
                df = ds_nwp.sel(x=xx, y=yy, z=zz).to_dataframe()
            else:
                df = ds_nwp.sel(x=xx, y=yy).to_dataframe()

            ## - Assume "gridded-chain" - ##
            vstation["filename"]    = "VIR" + vstation["vstation"]
            vstation["name"]        = "VIR" + vstation["vstation"]
            vstation['slope_angle'] = 0
        elif crs is not None:
            """Get coordinates in projection of nwp data"""
            proj_object = pyproj.Proj(crs)
            (xx,yy) = proj_object(vstation['lon'], vstation['lat'])
            if semidistributed:
                zz = vstation['elev']
                df = ds_nwp.sel(x=xx, y=yy, z=zz, method='nearest').to_dataframe()
            else:
                df = ds_nwp.sel(x=xx, y=yy, method='nearest').to_dataframe()
            vstation['easting']  = xx
            vstation['northing'] = yy
        else:
            raise ValueError("The vstations_csv file does not contain the keys 'easting' and 'northing' and the provided NWP data has no valid crs attribute for conversion.")

        ## - Only set EPSG code if available - ##
        try:
            vstation['epsg'] = str(crs.to_epsg())
        except KeyError:
            pass # no EPSG code available for this crs

        ## - Selecting fields/columns - ##
        if meteo_variables != "default":
            # fields_snp = ['TA', 'RH', 'VW', 'DW', 'VW_MAX', 'ISWR', 'ILWR', 'PSUM','TAU_CLD']
            fields_snp = meteo_variables.split(" ")
            df = df[fields_snp]

        ## - Cut time (optional to save time) - ##
        # prof_date = datetime.strptime(vstation['date'], "%Y-%m-%d")
        # df = df.loc[(df.index >= prof_date)]

        ## - Calculating cumulative precipitation for analysis (optional) - ##
        # df['PCUMSUM'] = df['PSUM'].cumsum()

        ## HACK: PRECIP SCALING FOR SLOPE SIMULATIONS WHILE ISSUE IS NOT IMPLEMENTED IN SNP
        df['PSUM'] = df['PSUM'] / np.cos(vstation['slope_angle'] * np.pi/180)

        ## - Parallel generation of SMET files - ##
        for p in running_procs[:]:
            if not p.is_alive():
                p.join()
                running_procs.remove(p)
        sema.acquire()
        proc = mp.Process(target=df2smet, args=(df, vstation, smet_dir, pbar, sema))
        running_procs.append(proc)
        proc.start()

    for proc in running_procs:
        proc.join()


def df2smet(df: object, vstation: dict, smet_dir: str, pbar=None, sema=None):
    """Write NETCDF dateset timeseries for single location to SMET."""

    fields_snp = df.columns[~df.columns.isin(['x', 'y', 'lat', 'lon'])].tolist()

    smet_path = os.path.join(smet_dir, vstation["filename"] + ".smet")
    smet_fields = ["timestamp"] + fields_snp

    ## - Header to SMET - ##
    # TODO: switch to blank header = {}? Need to check if this is more useful at all, then we need all params here 
    # TODO: Check for other setups!! Providing easting and northing with EPSG does not work for Tirol, setting easting/northing to -999 for now 
    header =  SMETParser.get_header(_smet_template_path)
    header['station_id']    = vstation["filename"]
    header['station_name']  = vstation['name']
    header['latitude']      = str(vstation['lat'])
    header['longitude']     = str(vstation['lon'])
    header['altitude']      = str(vstation['elev'])
    # header['easting']       = str(vstation['easting'])
    # header['northing']      = str(vstation['northing'])
    header['easting']       = str(-999)
    header['northing']      = str(-999)
    try:
        header['epsg']          = str(vstation["epsg"])
    except KeyError:
        pass
    header['tz']            = str(0)
    header['fields']        = " ".join(smet_fields)

    head_str = SMETParser.put_header(header)
    with open(smet_path, "w") as fsmet:
        fsmet.write(head_str + "\n")
        fsmet.write("[DATA]\n")

    ## - Data to SMET - ##
    if "DW" in fields_snp:
        df['DW']   = df['DW'].astype(int, errors='ignore')
    if "ISWR" in fields_snp:
        df['ISWR'] = df['ISWR'].astype(int, errors='ignore')
    if "ILWR" in fields_snp:
        df['ILWR'] = df['ILWR'].astype(int, errors='ignore')

    df.to_csv(smet_path, index=True, header=False, columns=fields_snp, float_format='%.3f', 
                encoding="utf-8", mode='a', sep='\t', na_rep="-999", date_format='%Y-%m-%dT%H:%M:%S')

    if pbar is not None:
        awio.model_chain_helpers.progress_bar(pbar['progress_counter'], pbar['lock'], pbar["stime"], pbar['ntasks'])
    if sema is not None:
        sema.release()


def generate_smet_single(
        meteo_source,
        out_path,
        xx=None, yy=None,
        lat=None, lon=None,
        epsg_code=None, nearest=False,
        datetime_start=None, datetime_end=None,
        variables: str='TA RH VW DW ISWR ILWR PSUM',
        toolchain: str='gridded-chain',
        domain=None, forecast: bool=False, verbose:bool=True
    ):
    """
    Generate a SMET file from an existing pre-processed netCDF file of NWP data for a specific set of coordinates and desired time period.
    This function will be able to query only those netCDF files that have been generated by the nwp_provider (or nwp_processor plugins 
    called from the nwp_provider).

    As opposed to the core nwp_provider function `open_dataset()`, which opens a comprehensive dataset for a specifically requested netCDF file,
    this function is designed to query NWP data for a specific set of coordinates and time period based on a meteo source but potentially 
    agnostic of the specific netCDF file that may contain the data. The NWP data for this point location will be written to a SMET file. 

    The function will attempt to extract the desired NWP data by looping through available domains if no specific domain is provided.

    For generating many SMET files, we advise to nwp_provider.open_dataset() and then generate_smets_for_pois() instead of this function!

    Parameters
    ----------
    meteo_source : str
        The source of the meteorological data, e.g., 'ecmwf-tirol'.
    x : float
        The easting coordinate of the location to extract data for. 
        Needs to be in the same CRS as the netCDF files unless `epsg_code` is provided.
    y : float
        The northing coordinate of the location to extract data for. 
        Needs to be in the same CRS as the netCDF files unless `epsg_code` is provided.
    datetime_start : str
        Start datetime of the desired time period parsable by pandas and xarray.
    datetime_end : str
        End datetime of the desired time period parsable by pandas and xarray.
    out_path : str
        The path to save the output SMET file.
    variables : str, optional
        A space-separated string of meteorological variables to extract.
    toolchain : str, optional
        The toolchain that hosts the existing netCDF file. The gridded-chain most likely hosts the most comprehensive set of pre-processed NWP data.
    domain : str, optional
        The domain from which to extract the NWP data. If not provided, the function will try to find suitable data from available domains.
    epsg_code : int, optional
        The EPSG code of the CRS of the x and y coordinates. If provided, coordinates will be transformed to the native CRS of the netCDF dataset. 
        It is strongly advised to provide an epsg code if no specific domain is provided, because different domains may contain NWP data in different CRS.
    forecast : bool, optional
        If True, forecast data will be extracted, otherwise nowcast data.
    verbose : bool, optional
        If True, additional information will be printed.

    Returns
    ------
    DataError.OK
        If full data has been retrieved.
    DataError.LOC_NA
        If the requested coordinates are not contained within any dataset.
    DataError.TIME_NA
        If the requested coordinates were found, but the data does not cover the time span.
    DateError.TIMESPAN_NA
        If partial data has been retrieved (i. e. part of the requested timespan).
    DataError.DATA_NA
        If no data files were found at all.

    Raises
    ------
    ValueError
        If the dataset can not be parsed.
    """

    if (lat or lon) and (xx or yy):
        sys.exit("[E] Can not use x, y together with lat, lon.")
    if not (xx and yy) and not (lat and lon):
        sys.exit("[E] Provide both values for either x and y, or lat and lon.")
    if verbose and not domain and not epsg_code and not (lat and lon):
        print("[w]  It is strongly advised to provide an epsg_code if you do not provide a domain from which to extract the NWP data!")

    _nwp_dir, _nwp_fname = _get_dir_fname(meteo_source, forecast, toolchain, domain)
    fpath = _get_nc_fpath(_nwp_dir, _nwp_fname, domain)

    ## read and extract from nc file:
    status = DataError.OK
    if not fpath:
        return DataError.DATA_NA
    for ncfile in fpath:
        try:
            ds = xr.open_dataset(ncfile)
        except:
            raise ValueError(f"Data file {ncfile} is misformatted.")

        if lat and lon:
            _spherical_epsg = 4326
            x_native, y_native = _convert_coordinates_crs(lon, lat, _spherical_epsg, ds)
        else:
            x_native, y_native = _convert_coordinates_crs(xx, yy, epsg_code, ds)

        if nearest: # ATT: will pick 1st found file
            ds = ds.sel(x=x_native, y=y_native, method="nearest")
        else:
            try:
                ds = ds.sel(x=x_native, y=y_native)
            except KeyError:
                if status < DataError.LOC_NA:
                    status = DataError.LOC_NA
                continue

        status = DataError.TIME_NA # if we reach this we have found a location match
        ds = ds[variables.split()]
        ds = ds.sel(time=slice(datetime_start, datetime_end))
        if len(ds['time']) == 0:
            continue
        elif ds['time'].min() > pd.to_datetime(datetime_start) or \
            ds['time'].max() < pd.to_datetime(datetime_end):
            status = DataError.TIMESPAN_NA # return this data but flag as incomplete
        else:
            status = DataError.OK
        df = ds.to_dataframe()
        meta = {
            "name":     f"{xx}_{yy}",
            "filename": os.path.splitext(os.path.basename(out_path))[0],
            "lat":      lat or str(-999),  # TODO: handle these meta information more informatively
            "lon":      lon or str(-999),
            "elev":     str(-999),
        }
        if epsg_code:
            meta["epsg"] = str(epsg_code) # can't be nodata, but can be empty if defined in ini

        dir_path = os.path.dirname(out_path)
        df2smet(df, meta, dir_path)
        return status
    return status # no match, either no location at all or no times found

def _get_dir_fname(meteo_source, forecast, toolchain, domain):
    """Get directory and filename of pre-processed netcdf file with NWP data"""
    if forecast:
        meteo_source_fc = meteo_source  # required for subsequent eval
        _nwp_dir = eval(_NWP_DIR_FC)
        if domain:
            _nwp_fname = eval(_NWP_FNAME_FC)
        else:
            _nwp_fname = eval(_NWP_FNAME_FC.replace("{domain}", "*"))
    else:
        meteo_source_nc = meteo_source
        _nwp_dir = eval(_NWP_DIR_NC)
        if domain:
            _nwp_fname = eval(_NWP_FNAME_NC)
        else:
            _nwp_fname = eval(_NWP_FNAME_NC.replace("{domain}", "*"))
    return _nwp_dir, _nwp_fname


def _get_nc_fpath(_nwp_dir, _nwp_fname, domain) -> list[str]:
    """get netcdf file or list of potential files"""
    if domain:
        fpath = [os.path.join(_nwp_dir, _nwp_fname)]
        fpath = [fp for fp in fpath if os.path.exists(fp)]
    else:
        fpath = glob.glob(os.path.join(_nwp_dir, _nwp_fname))
    return fpath

def _convert_coordinates_crs(xx, yy, source_epsg, ds):
    """Transform between any epsg code and the data file's crs"""
    if not source_epsg:
        return xx, yy

    ds_crs = pyproj.CRS(ds.attrs["crs"]) # TODO: try other standard netCDF methods for different sources
    target_epsg = ds_crs.to_epsg()

    if source_epsg == target_epsg:
        return xx, yy

    transformer = pyproj.Transformer.from_crs(f"epsg:{source_epsg}", f"epsg:{target_epsg}", always_xy=True)
    aa, bb = transformer.transform(xx, yy)
    return aa, bb

def _get_domain_from_NWP_FNAME(filename: str, throw: bool=True) -> str:
    fname_parts_nc = _NWP_FNAME_NC.split("'")[1].split("{domain}")
    fname_parts_fc = _NWP_FNAME_FC.split("'")[1].split("{domain}")
    if fname_parts_nc[0] in filename:
        domain = os.path.basename(filename).split(fname_parts_nc[0])[-1].split(fname_parts_nc[1])[0]
    elif fname_parts_fc[0] in filename:
        domain = os.path.basename(filename).split(fname_parts_fc[0])[-1].split(fname_parts_fc[1])[0]
    else:
        if throw:
            raise ValueError("Cannot identify domain from filename. filename does not seem to follow the standard naming convention of _NWP_FNAME_NC/_NWP_FNAME_FC!")
    return domain


def prepend_last_timestep(ds_nwp: xr.Dataset, ds_nwp_prior: xr.Dataset):
    """
    Prepend the last timestep from `ds_nwp_prior` to `ds_nwp`.
    """
    last_timestep_prior = ds_nwp_prior.sel(time=ds_nwp_prior.time.max())
    ds_nwp_updated = xr.concat([last_timestep_prior, ds_nwp], dim="time")

    return ds_nwp_updated



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="SMET file generator for NWP data.")
    subparsers = parser.add_subparsers(dest="command", help="Choose the operation to perform (generate_smets_for_pois or generate_smet_single)")

    # Parser for generate_smets_for_pois
    parser_pois = subparsers.add_parser("generate_smets_for_pois", help="Generate SMET files for a list of POIs.")
    parser_pois.add_argument('ds_nwp', help="Path to NWP dataset or xarray dataset")
    parser_pois.add_argument('df_vstations', help="Path to CSV file or dataframe of POIs")
    parser_pois.add_argument('smet_dir', help="Output directory for SMET files")
    parser_pois.add_argument('--meteo_variables', default="default", help="Meteo variables included in the SMET file")
    parser_pois.add_argument('--crs', default=None, help="CRS information (optional)")
    parser_pois.add_argument('--ncpus', default=1, type=int, help="Number of CPUs for parallel processing")

    # Parser for generate_smet_single
    parser_single = subparsers.add_parser("generate_smet_single", help="Generate a single SMET file from NWP data.")
    parser_single.add_argument('meteo_source', help="Meteo source string, e.g., ecmwf-tirol")
    parser_single.add_argument('x', type=float, help="Easting coordinates in the NWP data's CRS or EPSG CRS")
    parser_single.add_argument('y', type=float, help="Northing coordinates in the NWP data's CRS or EPSG CRS")
    parser_single.add_argument('lat', type=float, help="Latitude in geographic coordinates")
    parser_single.add_argument('lon', type=float, help="Longitude in grographic coordinates")
    parser_single.add_argument('datetime_start', help="Start datetime of SMET file (YYYY-MM-DD HH:MM:SS)")
    parser_single.add_argument('datetime_end', help="End datetime of SMET file (YYYY-MM-DD HH:MM:SS)")
    parser_single.add_argument('out_path', help="Output path for the generated SMET file")
    parser_single.add_argument('--variables', default='TA RH VW DW ISWR ILWR PSUM', help="NWP variables to include in the SMET file")
    parser_single.add_argument('--toolchain', default='gridded-chain', help="Toolchain for hosting the NWP data")
    parser_single.add_argument('--domain', default=None, help="Specific domain for NWP data")
    parser_single.add_argument('--epsg_code', type=int, default=None, help="EPSG code of the CRS of the x and y coordinates")  # TODO fherla: run with CRS info instead!
    parser_single.add_argument('--nearest', action='store_true', help="Whether to choose nearest grid point")
    parser_single.add_argument('--forecast', action='store_true', help="Whether to extract forecast data")
    parser_single.add_argument('--verbose', action='store_true', help="Enable verbose output")

    args = parser.parse_args()

    if args.command == "generate_smets_for_pois":
        generate_smets_for_pois(
            ds_nwp=args.ds_nwp, 
            df_vstations=args.df_vstations, 
            smet_dir=args.smet_dir, 
            meteo_variables=args.meteo_variables, 
            crs=pyproj.CRS(args.crs) if args.crs else None, 
            ncpus=args.ncpus
        )
    elif args.command == "generate_smet_single":
        # Call generate_smet_single function with parsed arguments
        generate_smet_single(
            meteo_source=args.meteo_source,
            xx=args.x,
            yy=args.y,
            lat=args.lat,
            lon=args.lon,
            epsg_code=args.epsg_code,
            nearest=args.nearest,
            datetime_start=args.datetime_start,
            datetime_end=args.datetime_end,
            out_path=args.out_path,
            variables=args.variables,
            toolchain=args.toolchain,
            domain=args.domain,
            forecast=args.forecast,
            verbose=args.verbose
        )
    else:
        parser.print_help()
