import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="aw-data-provider",
    version="0.0.1",
    license="AGPL",
    author="AWSOME Core Team",
    description="Provide data to other modules and toolchains within AWSOME framework",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/avalanche-warning/data/provider",
    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={
        'nwp_provider': ['templates/template.smet'],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.10",
    install_requires=[
        "pandas",
        "pymssql",
        "pyproj",
        "shapely",
        "sqlalchemy",
        "xarray",
        "bottleneck",
        "configparser",
        "argparse"
    ]
)
