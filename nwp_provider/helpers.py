import os
import pyproj
import numpy as np
import pandas as pd
import xarray as xr
from shapely.wkt import loads, dumps
from shapely.geometry import Point, LineString, Polygon, MultiPoint, MultiLineString, MultiPolygon, GeometryCollection

_NWP_DIR_NC = "os.path.expanduser(f'~snowpack/{toolchain}/data/{meteo_source_nc}')"  # needs to be `eval`uated within the correct scope.
_NWP_FNAME_NC = "f'nwp_nowcast-{domain}.nc'"
_NWP_DIR_FC = "os.path.expanduser(f'~snowpack/{toolchain}/data/{meteo_source_fc}')"
_NWP_FNAME_FC = "f'nwp_forecast-{domain}.nc'"

def interpolate_temporally(ds: xr.Dataset, threshold_hours=50, throw=False, **kwargs):
    threshold_hours = np.timedelta64(threshold_hours, 'h')
    if ds['time'].shape[0] > 0:
        diff_hours = ds['time'].diff("time").astype('timedelta64[h]')
        large_gaps = diff_hours > threshold_hours
        if large_gaps.any():
            max_gap = diff_hours.max().values
            msg = f"Detected a gap of {max_gap} hours, which exceeds the threshold of {threshold_hours.astype('int')}h."
            if throw:
                raise ValueError(msg)
            else:
                print(f"[W]  {msg}")
        if (diff_hours > np.timedelta64(1, 'h')).any():
            complete_ts = pd.date_range(start=ds['time'].values.min(),
                                        end=ds['time'].values.max(), freq='H')
            missing_times = np.setdiff1d(complete_ts, ds['time'].values)
            missing_times_str = "\n     ".join([str(t) for t in missing_times])
            print(f"[i]  Missing time steps:\n     {missing_times_str}")
            print(f"     Interpolating all gaps smaller than {threshold_hours}")
            ds = ds.reindex(time=complete_ts)
            ds = ds.interpolate_na(dim='time', method='linear', max_gap=threshold_hours)
    return ds


def add_crs_attribute(ds: xr.Dataset, crs: str, **kwargs):
    ds = ds.assign_attrs(crs=pyproj.CRS(crs).to_wkt())
    return ds



def transform_wkt_crs(wkt: str, source_crs: str, target_crs: str) -> str:
    """
    Does not work!! Fix!

    Transforms a WKT geometry from one CRS to another, supporting multi-part geometries
    like MultiPoint and GeometryCollection.

    Parameters
    ----------
    wkt : str
        The input geometry in Well-Known Text (WKT) format.
    source_crs : str
        The source CRS, provided in any format accepted by pyproj.CRS
        (e.g., an EPSG code, PROJ string, or WKT string).
    target_crs : str
        The target CRS, provided in any format accepted by pyproj.CRS.

    Returns
    -------
    str
        The transformed geometry in Well-Known Text (WKT) format.

    Example
    -------
    >>> wkt = "GEOMETRYCOLLECTION(MULTIPOINT((18.95574212 69.45771592), (19.46197429 69.44546150)), POINT(20.0 70.0))"
    >>> source_crs = "EPSG:4326"  # WGS84
    >>> target_crs = "EPSG:25833"  # UTM Zone 33N
    >>> transform_wkt_to_crs(wkt, source_crs, target_crs)
    'GEOMETRYCOLLECTION (MULTIPOINT ((430737.14 7711506.98), (469602.73 7708898.95)), POINT (500000.00 7763065.78))'
    """
    raise NotImplementedError
    # Parse the CRS inputs
    source_crs = pyproj.CRS(source_crs)
    target_crs = pyproj.CRS(target_crs)
    geometry = loads(wkt)

    transformer = pyproj.Transformer.from_crs(source_crs, target_crs, always_xy=True)

    def transform_geometry(geometry):
        """
        Recursively transform a Shapely geometry into the target CRS.
        """
        if geometry.is_empty:
            return geometry

        if geometry.geom_type == "GeometryCollection":
            transformed_geometries = [transform_geometry(geom) for geom in geometry.geoms]
            return GeometryCollection(transformed_geometries)

        elif geometry.geom_type.startswith("Multi"):
            # Handle MultiPoint, MultiLineString, MultiPolygon
            transformed_geometries = [transform_geometry(geom) for geom in geometry.geoms]
            return geometry.__class__(transformed_geometries)

        else:
            # Handle single geometries like Point, LineString, Polygon
            transformed_coords = [transformer.transform(x, y) for x, y in geometry.coords]
            return geometry.__class__(transformed_coords)

    transformed_geometry = transform_geometry(geometry)
    return dumps(transformed_geometry)

