"""
This module provides snow profile observations in CAAML format for specific domains within AWSOME and prepares them
to initialize SNOWPACK simulations. Currently, it supports the Lawis and Regobs platforms to fetch snow profiles. Micropren
observations will be included soon.

The module provides the interface for specific plugins to fetch profiles from different sources. Adding a new plugin only requires small changes
within this data provider, the toolchain itself stays untouched. The provider can be run as a script or imported into Python.

Functionality:
--------------
- Fetch snow profiles or similar snow point measurements from a specific platform / data source
- Prepare profiles (CAAML files) for SNOWPACK simulations

Usage:
------
1. Interactive Python usage:
    >>> import profile_provider as awprof_provider

2. Script:
    Script usage will invoke the function `get_and_prepare_profiles_for_snp` by
    $ python3 profile_provider.py domain [--domain-path]


Dependencies:
-------------
The module relies on access to profile data through custom processor plugins.
So for adding new profile sources to AWSOME, please
1. provide a processor plugin that fetches the profiles from the platform / database / ...
   (Ideally the new plugin only needs similar config parameters than exisiting plugins)
3. add specific settings for your profile source within 'get_and_prepare_profiles_for_snp()'
"""

import os
import sys
import argparse
import configparser
import subprocess
import glob

from datetime import datetime, timedelta, date
import pandas as pd
import xarray as xr

from awset.domain_settings import get, getattribute
from snowpacktools.caaml import caamlv6_processor
from awset.domain_settings import DEFAULT_DOMAIN_PATH

_date_format = '%Y-%m-%d'

def _get_date_opera(domain: str, toolchain: str, _domain_path):
    """Flexible operational date for virtual operation"""

    if get([toolchain, "run", "date_opera"], domain, cfgpath=_domain_path) == "TODAY":
        date_opera  = date.today()
    else:
        date_opera  = datetime.strptime(get([toolchain, "run", "date_opera"], domain, cfgpath=_domain_path), _date_format).date()

    date_season_end = datetime.strptime(get([toolchain, "run", "season_end"], domain, cfgpath=_domain_path), _date_format).date()
    if date_opera <= date_season_end:
        date_opera  = datetime.strftime(date_opera, _date_format)
    else:
        date_opera  = datetime.strftime(date_season_end, _date_format)
    return date_opera


def get_and_prepare_profiles_for_snp(domain: str, toolchain:str, _domain_path = DEFAULT_DOMAIN_PATH):
    """Download and prepare snow profiles for snp simulations"""
    
    """Select profile processor"""
    if get([toolchain, "profile", "source"], domain, cfgpath=_domain_path) == 'lawis':
        profile_processor = os.path.expandvars("$AWSOME_BASE/data/fetchers/profile-obs/lawis_processor.py")    
    elif get([toolchain, "profile", "source"], domain, cfgpath=_domain_path) == 'regobs':
        profile_processor = os.path.expandvars("$AWSOME_BASE/data/fetchers/profile-obs/regobs_processor.py")
    else:
        raise("[E]  Selected profile processor not implemented.")

    """Define period used to search for observed snow profiles"""
    date_opera        = datetime.strptime(_get_date_opera(domain, toolchain, _domain_path), _date_format)
    search_prof_start = (date_opera - timedelta(days=int(get([toolchain, "profile", "lead_time"], domain, cfgpath=_domain_path)))).date()
    season_start      = datetime.strptime(get([toolchain, "run", "season_start"], domain, cfgpath=_domain_path), _date_format).date()
    sdate             = max(search_prof_start, season_start) # Take whatever date is later
    sdate             = datetime.strftime(sdate, _date_format)
    edate             = datetime.strftime(date_opera, _date_format)
    
    prof_dir       = os.path.expanduser(f"~snowpack/snowobs-chain/data/profiles/{domain}")
    ncpus          = "1"
    if getattribute([toolchain, "run", "ncpus"], "enabled", domain, cfgpath=_domain_path) == "true":
        ncpus      = get([toolchain, "run", "ncpus"], domain, cfgpath=_domain_path)
    identifier     = ""
    if getattribute([toolchain, "profile", "identifier"], "enabled", domain, cfgpath=_domain_path) == "true":
        identifier = get([toolchain, "profile", "identifier"], domain, cfgpath=_domain_path)

    subprocess.call(["python3", profile_processor, prof_dir, sdate, edate, '--identifier', identifier, '--ncpus', ncpus])

    """Validate and prepare profiles for SNOWPACK simulations (+ add Monti density)"""
    print('[i]  Validating and preparing observed snow profiles (e.g. adding density a la Monti).')
    profs_downloaded = sorted(glob.glob(os.path.join(prof_dir,"*.caaml")))
    for prof_path in profs_downloaded:
        caamlv6_processor.validate_and_prepare_for_snp(prof_path,prof_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('domain', help='The domain for which to get NWP data')
    parser.add_argument('toolchain', choices=['gridded-chain', 'snowobs-chain'])
    parser.add_argument('--domain-path', default=DEFAULT_DOMAIN_PATH, help='Where to find the domain.xml file')
    args = vars(parser.parse_args())
    get_and_prepare_profiles_for_snp(args['domain'], args['toolchain'], args['domain-path'])