"""
This module provides the raw snow-cover simulations from specific domains within AWSOME.

Currently, the snowsim_provider can only provide the directory path that contains the .pro/.smet files of a specific simulation.
See `snowsim_dir?` for more info.
"""

import os
import json
import logging
from awset import get, getattribute, DEFAULT_DOMAIN_PATH

def snowsim_dir(domain:str, toolchain:str='gridded-chain', domain_path:str=DEFAULT_DOMAIN_PATH):
    """
    Retrieve the directory path for existing snow-cover simulations.

    This function determines the directory path for snow-cover simulations based on the domain and toolchain specified.
    It first checks for AWSOME's domain.xml config file, but then also checks environment variables or local files to make 
    it usable outside of an AWSOME context.

    Parameters:
    -----------
    domain : str
        The domain name for which the snow-cover simulations are being retrieved.
    toolchain : str, optional
        The toolchain name (to be used for use within AWSOME)
    domain_path : str, optional
        The path to the directory containing domain configuration files.

    Returns:
    --------
    str
        The path to the snow-cover simulation directory that contains the .pro/.smet files

    Raises:
    -------
    ValueError
        If the function cannot determine the snow-cover simulation directory.

    Notes:
    ------
    The function follows these steps to determine the snow-cover simulation directory:
    1. Checks if `snp-directory` is enabled in the domain.xml file (section toolchain/output/)
    2. If not, checks if `directory` is enabled in the domain.xml file (section toolchain/output)
    3. If both `snp-directory` and `directory` are disabled, it constructs the default path at ~snowpack/{toolchain}/snp-{domain}
    4. If domain.xml does not exist (i.e., no AWSOME context), it checks for an environment variable `SNOWSIM_DIR`.
    5. If the environment variable is not set, it checks for the files `./SNOWSIM_DIR.json` or `~/.SNOWSIM_DIR.json`.
       The file can list a dictionary of paths with keys describing the different domains, e.g., {"domain1": "path/to/dir"} 
    6. If the file is not found, it checks the current directory for any .pro or .smet files.
    7. If none of the above methods succeed, it raises a ValueError.

    Examples:
    ---------
    >>> snowsim_dir('mydomain', 'gridded-simulations')
    '/opt/snowpack/gridded-simulations/output/snp-mydomain/'

    """

    ## non-AWSOME use case: domain.xml does not exist
    if not os.path.exists(f"{domain_path}/{domain}.xml"):
        
        # Check environment variable
        snp_dir = os.getenv('SNOWSIM_DIR')

        # Check for SNOWSIM_DIR.json
        if snp_dir is None:
            snp_dir_file_path = None
            if os.path.exists("./SNOWSIM_DIR.json"):
                snp_dir_file_path = "./SNOWSIM_DIR.json"
            if os.path.exists(os.path.expanduser("~/.SNOWSIM_DIR.json")):
                snp_dir_file_path = os.path.expanduser("~/.SNOWSIM_DIR.json")
            if snp_dir_file_path:
                try:
                    with open(snp_dir_file_path, 'r') as snp_dir_file:
                        snp_dir_dict = json.load(snp_dir_file)
                        snp_dir = snp_dir_dict.get(domain)
                        if snp_dir is None:
                            raise ValueError(f"Domain '{domain}' not found in {snp_dir_file_path}")
                except json.JSONDecodeError:
                    logging.error(f"Failed to decode JSON from {snp_dir_file_path}")
                    raise
                except Exception as e:
                    logging.error(f"An error occurred while reading {snp_dir_file_path}: {str(e)}")
                    raise
        # Check current directory for whether pro and smet files exist
        if snp_dir is None:
            cwd = os.getcwd()
            cwd_files = os.listdir(cwd)
            # Filter files with .pro and .smet extensions
            nmax = min(len(cwd_files), 50)
            pro_files = [file for file in cwd_files[:nmax] if file.endswith('.pro')]
            smet_files = [file for file in cwd_files[:nmax] if file.endswith('.smet')]
            
            if pro_files or smet_files:
                snp_dir = cwd
        
        if snp_dir is None:
            raise ValueError("Don't know where to find snowsim_dir. Please review the function documentation.")
        else:
            return snp_dir
    
    ## domain.xml: snp-directory set
    elif getattribute([toolchain, "output", "snp-directory"], "enabled", domain, domain_path) == "true":
        return os.path.join(get([toolchain, "output", "snp-directory"], domain, domain_path, throw=True), "")
    
    ## domain.xml: directory set
    elif getattribute([toolchain, "output", "directory"], "enabled", domain, domain_path) == "true":
        return os.path.join(get([toolchain, "output", "directory"], domain, domain_path, throw=True), 
                               f"snp-{domain}", "")
    
    ## domain.xml: snp-directory and directory are both disabled
    elif getattribute([toolchain, "output", "snp-directory"], "enabled", domain, domain_path) == "false" and \
    getattribute([toolchain, "output", "directory"], "enabled", domain, domain_path) == "false":
        return os.path.expanduser(f"~snowpack/{toolchain}/output/snp-{domain}/")

    else:
        raise ValueError("domain.xml file exists, but seems incorrectly formatted. Please revise!")
        

    